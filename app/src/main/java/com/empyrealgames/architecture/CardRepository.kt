package com.empyrealgames.architecture

import android.os.AsyncTask
import androidx.lifecycle.LiveData

class CardRepository(private var cardDao: CardDao) {

    var allCards: LiveData<List<Card>> = cardDao.all()

    fun insert(card: Card) {
        InsertCardAsyncTask(cardDao).execute(card)
    }

    private class InsertCardAsyncTask(cardDao: CardDao) : AsyncTask<Card, Void, Int>() {
        var cardDao: CardDao? = null
        init {
            this.cardDao = cardDao
        }

        override fun doInBackground(vararg params: Card?): Int {
            cardDao!!.insertAll(params[0]!!)
            return 0


        }
    }

}