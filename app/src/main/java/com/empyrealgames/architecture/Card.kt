package com.empyrealgames.architecture

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Card")
data class Card(var userId:Int, @PrimaryKey val id: Int = 0, var title: String, var body:String)