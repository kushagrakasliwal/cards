package com.empyrealgames.architecture

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class CardViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: CardRepository

    var allCards: LiveData<List<Card>> = MutableLiveData()

    init {
        val cardDao = CardDatabase.getCardDatabase(application).cardDao()
        repository = CardRepository(cardDao)
        allCards = repository.allCards

        fetchData()
    }

    fun insert(card: Card) {
        repository.insert(card)
    }


    private fun fetchData(){
        val gson = GsonBuilder()
            .setLenient()
            .create()

        val retrofit = Retrofit.Builder()
            .baseUrl("https://jsonplaceholder.typicode.com/")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        val cardJsonApi = retrofit.create(CardJsonApi::class.java)

        val call = cardJsonApi.getCards()


        call.enqueue(object : Callback<List<Card>> {
            override fun onFailure(call: Call<List<Card>>, t: Throwable) {
                println("Failure "  + t.message)
            }

            override fun onResponse(call: Call<List<Card>>, response: Response<List<Card>>) {
                if(response.isSuccessful){
                    val cards = response.body()
                    for(card in cards!!){
                        insert(card)
                    }
                }
            }
        })

    }

}

