package com.empyrealgames.architecture

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView


import androidx.fragment.app.Fragment

class ScreenSlidePageFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val rootView = inflater.inflate(
            R.layout.fragment_screen_slide_page, container, false
        )
        val cardTextView = rootView.findViewById<TextView>(R.id.cardTextView)
        cardTextView.text = arguments!!.getString("DATA")
        return rootView
    }

}
