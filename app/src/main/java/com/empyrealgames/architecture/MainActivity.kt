package com.empyrealgames.architecture


import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.max


const  val data: String = "DATA"
class MainActivity : AppCompatActivity() {


    private var cardsLength: Int = 0

    private lateinit var cardViewModel: CardViewModel
    private var idList: ArrayList<Int> = ArrayList()
    private var textList: ArrayList<String> = ArrayList()


    private var pagerAdapter: PagerAdapter? = null
    private fun askPermissions() {
        val permissions = arrayOf(
            "android.permission.ACCESS_NETWORK_STATE",
            "android.permission.INTERNET"
        )
        val requestCode = 200
        requestPermissions(permissions, requestCode)
    }

    override fun onCreate(savedInstanceState: Bundle?) {


        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        askPermissions()

        cardViewModel = ViewModelProviders.of(this).get(CardViewModel::class.java)
        cardViewModel.allCards.observe(this, object : Observer<List<Card>> {
            override fun onChanged(t: List<Card>) {
                cardsLength = t.size
                idList = ArrayList()
                textList = ArrayList()
                for (i in 0 until cardsLength) {
                    idList.add(t[i].id)
                    textList.add(t[i].title)
                }

                pagerAdapter?.notifyDataSetChanged()

            }
        })
        val mPager: ViewPager = findViewById(R.id.pager)
        mPager.setPageTransformer(true, ZoomOutPageTransformer())
        pagerAdapter = ScreenSlidePagerAdapter(supportFragmentManager)
        mPager.adapter = pagerAdapter
      //  PopulateDbAsyncTask(cardViewModel).execute()




        previousButton.setOnClickListener() { pager.currentItem -= 1 }
        nextButton.setOnClickListener() {
            if (nextButton.text != ("Finish")) {
                pager.currentItem = pager.currentItem + 1
            }
        }
        restartButton.setOnClickListener(){pager.currentItem = 0}

        pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                when (position) {
                    0 -> {
                        nextButton.isEnabled = true
                        nextButton.isVisible = true
                        previousButton.isEnabled = false
                        previousButton.isVisible = false
                        nextButton.text = resources.getString(R.string.next_button)
                    }

                    cardsLength - 1 -> {
                        nextButton.text = resources.getString(R.string.finish_button)
                    }

                    else -> {
                        previousButton.isEnabled = true
                        previousButton.isVisible = true
                        nextButton.text = resources.getString(R.string.next_button)

                    }
                }
                pageNumberTextView.text = idList[position].toString()
            }
        })

    }


    private inner class ScreenSlidePagerAdapter(fragmentManager: FragmentManager) :
        FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
        override fun getCount(): Int {
            return cardsLength
        }

        override fun getItem(position: Int): Fragment {
            val fragment = ScreenSlidePageFragment()
            val b = Bundle()
            b.putString(data, textList[position])
            fragment.arguments = b
            return fragment
        }

        override fun getItemPosition(`object`: Any): Int {
            return super.getItemPosition(`object`)
        }
    }

    class ZoomOutPageTransformer : ViewPager.PageTransformer {

        override fun transformPage(view: View, position: Float) {
            view.apply {
                val pageWidth = width
                val pageHeight = height
                when {
                    position < -1 -> { // [-Infinity,-1)
                        // This page is way off-screen to the left.
                        alpha = 0f
                    }
                    position <= 1 -> { // [-1,1]
                        // Modify the default slide transition to shrink the page as well
                        val scaleFactor = max(0.85f, 1 - Math.abs(position))
                        val vertMargin = pageHeight * (1 - scaleFactor) / 2
                        val horzMargin = pageWidth * (1 - scaleFactor) / 2
                        translationX = if (position < 0) {
                            horzMargin - vertMargin / 2
                        } else {
                            horzMargin + vertMargin / 2
                        }

                        // Scale the page down (between MIN_SCALE and 1)
                        scaleX = scaleFactor
                        scaleY = scaleFactor

                        // Fade the page relative to its size.
                        alpha = (0.5f +
                                (((scaleFactor - 0.85f) / (1 - 0.85f)) * (1 - 0.5f)))
                    }
                    else -> { // (1,+Infinity]
                        // This page is way off-screen to the right.
                        alpha = 0f
                    }
                }
            }
        }
    }

}
