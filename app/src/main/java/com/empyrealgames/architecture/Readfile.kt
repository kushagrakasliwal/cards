package com.empyrealgames.architecture

import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.URL


class ReadFile {
    fun readFile(path: String?): String {
        val answer = StringBuilder("")
        var ans = ""
        try {
            val url = URL(path)
            val `in` =
                BufferedReader(InputStreamReader(url.openStream()))
            var curr: String?
            while (`in`.readLine().also { curr = it } != null) answer.append(curr)
            ans = answer.toString()
            ans = ans.substring(ans.indexOf('/') + 1)
            `in`.close()
        } catch (e: IOException) {
        }
        println(ans)
        return ans
    }
}