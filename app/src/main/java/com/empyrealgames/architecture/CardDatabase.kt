package com.empyrealgames.architecture


import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


@Database(entities = [(Card::class)], version = 10)
abstract class CardDatabase : RoomDatabase() {


    abstract fun cardDao(): CardDao

    companion object {
        private var INSTANCE: CardDatabase? = null

        private val roomCallBack = object : RoomDatabase.Callback() {

        }

        fun getCardDatabase(context: Context): CardDatabase {

            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }

            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    CardDatabase::class.java,
                    "card_database"
                ).fallbackToDestructiveMigration()
                    .addCallback(roomCallBack)
                    .build()
                INSTANCE = instance
                return instance
            }
        }

    }




}