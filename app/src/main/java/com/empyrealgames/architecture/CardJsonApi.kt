package com.empyrealgames.architecture

import retrofit2.Call
import retrofit2.http.GET


interface CardJsonApi  {
    @GET("posts")
    fun getCards():Call<List<Card>>
}