package com.empyrealgames.architecture

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
 interface CardDao {
    @Query("SELECT * FROM Card ")
    fun all ():LiveData<List<Card>>

    @Insert
    fun insertAll(vararg card: Card)

}